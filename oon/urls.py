"""oon URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
"""
# Django imports
from django.conf.urls import include, url
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.urls import path
# from drf_yasg import openapi
# from drf_yasg.views import get_schema_view
from rest_framework import routers, permissions
from rest_framework_simplejwt import views as jwt_views
from rest_framework_simplejwt.authentication import JWTAuthentication

router = routers.DefaultRouter()

# schema_view = get_schema_view(
#     openapi.Info(
#         title="OON API",
#         default_version='v0.0.1',
#         description="OON API",
#     ),
#     public=True,
#     permission_classes=(permissions.AllowAny,),
#     authentication_classes=(JWTAuthentication,)
# )

urlpatterns = [

    # provide the most basic login/logout functionality
    path('', include(router.urls)),
    url(r'^login/$', auth_views.LoginView.as_view(template_name='core/login.html'),
        name='core_login'),
    url(r'^logout/$', auth_views.LogoutView.as_view(), name='core_logout'),

    path('api/token/', jwt_views.TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/token/refresh/', jwt_views.TokenRefreshView.as_view(), name='token_refresh'),

    # enable the admin interface
    url(r'^admin/', admin.site.urls),
    # enable swagger
    # url(r'^swagger(?P<format>\.json|\.yaml)$', schema_view.without_ui(cache_timeout=0), name='schema-json'),
    # url(r'^swagger/$', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    # url(r'^redoc/$', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),

    # api
    path('api/', include(('oon.app.api.urls', 'oon.app.api'), namespace='api')),
]
