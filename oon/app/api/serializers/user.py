from rest_framework import serializers
from rest_framework.validators import UniqueValidator

from oon.app.models.user import User
from .base import BaseSerializer


class UserSerializer(BaseSerializer):
    password = serializers.CharField(style={"input_type": "password"}, write_only=True)

    class Meta(BaseSerializer.Meta):
        model = User
        fields = BaseSerializer.Meta.fields + (
            'id',
            'email',
            'first_name',
            'last_name',
            'profile_picture',
            'phone_number',
            'username',
            'password')
        # validators = [
        #     RequiredValidator(
        #         fields=('email', 'first_name', 'last_name')
        #     )
        # ]
        extra_kwargs = {
            'email': {'validators': [UniqueValidator(queryset=User.objects.all(), message="Email already exists")],
                      'required': True},
            'first_name': {'required': True},
            'last_name': {'required': True}
        }

    def create(self, validated_data):
        password = validated_data.pop('password')
        user_obj = User(**validated_data)
        user_obj.set_password(password)
        user_obj.save()
        return validated_data
