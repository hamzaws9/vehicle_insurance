from django.urls import path

from .views import user

urlpatterns = [
    # user
    path('users/', user.UserList.as_view(), name='user-list'),
    path('users/<int:pk>/', user.UserDetail.as_view(), name='user-profile'),
]
