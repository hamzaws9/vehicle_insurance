from django.contrib.auth.models import AbstractUser
from django.db import models

from .base import BaseModel


class User(AbstractUser, BaseModel):
    phone_number = models.CharField(max_length=20, unique=True)

    class Meta:
        db_table = "user"

    REQUIRED_FIELDS = ["first_name", "last_name", "email", "phone_number"]

    def __str__(self):
        return self.email
