from django.db import models


class BaseModel(models.Model):
    # not using auto_add and auto_add_now parameters
    # cuz they reported to be buggy
    created_at = models.DateTimeField(editable=False, auto_now_add=True)
    updated_at = models.DateTimeField(editable=False, auto_now=True)

    # def save(self, *args, **kwargs):
    #     if not self.created_at:
    #         self.created_at = timezone.now()
    #
    #     self.updated_at = timezone.now()
    #     return super(BaseModel, self).save(*args, **kwargs)

    class Meta:
        abstract = True
