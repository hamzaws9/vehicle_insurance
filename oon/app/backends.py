from django.contrib.auth import get_user_model
from django.db.models import Q

from django.contrib.auth.backends import BaseBackend


class AuthenticationBackend(BaseBackend):

    def get_user(self, user_id):
        my_user_model = get_user_model()
        try:
            return my_user_model.objects.get(pk=user_id)
        except my_user_model.DoesNotExist:
            return None

    def authenticate(self, request, username=None, password=None):
        my_user_model = get_user_model()
        try:
            user = my_user_model.objects.get(
                Q(username=username) | Q(email=username) | Q(phone_number=username)
            )
        except my_user_model.DoesNotExist:
            return None

        return user if user.check_password(password) else None
