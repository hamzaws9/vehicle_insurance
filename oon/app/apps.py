from django.apps import AppConfig


class ApplicationConfig(AppConfig):
    name = 'oon.app'
    verbose_name = "OONApp"
