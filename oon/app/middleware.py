from django.utils.deprecation import MiddlewareMixin
from rest_framework.response import Response


class ResponseCustomMiddleware(MiddlewareMixin):
    def __init__(self, *args, **kwargs):
        super(ResponseCustomMiddleware, self).__init__(*args, **kwargs)

    def process_template_response(self, request, response):
        exclude_paths = ['/swagger/', '/redoc/']
        if request.path not in exclude_paths:
            if not response.is_rendered and isinstance(response, Response):
                if isinstance(response.data, dict):
                    message = response.data.get('message', '')
                    error_type = None
                    # you can add you logic for checking in status code is 2** or 4**.
                    status = 'unknown'
                    if response.status_code // 100 == 2:
                        status = 'success'
                        if 'data' not in response.data:
                            response.data = {'data': response.data}
                    elif response.status_code // 100 == 4 or response.status_code // 100 == 5:
                        status = 'error'
                        error_type = response.data.get('error_type')
                        response.data = {'data': {}}
                    code = response.status_code
                    meta = {'status': status, 'code': code, 'message': message}
                    if error_type:
                        meta['error_type'] = error_type
                    response.data.setdefault('meta', meta)
        return response
