# oon
OON insurance

### Setting up
- Install `poetry` and run `poetry install`
- Copy `env.example` to `.env` and change the db values
- install `pre-commit` and run `pre-commit install`
- TODO

## IMPORTANT
 - Use semantic commit messages as [highlighted here](https://gist.github.com/joshbuchea/6f47e86d2510bce28f8e7f42ae84c716)
 - Each feature/bug should be implemented in a new branch that can be merged later into `develop` via a PR
 - `develop` branch will always feature the latest code so every feature/bug branch should be created from it. `master` is for releases
